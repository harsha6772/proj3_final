var mymap;
var satellite;
var osm;
var basemaps;
var drawnItems;

$(document).ready(function() {

    mymap = L.map('map_id',{
        center:[18.74647, 83.401877],
        zoom:5,
        minZoom:2,
        maxZoom: 18,
        layersControl:true,
        //drawControl: true,
    });

    osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap) //same as "mymap.addLayer(osm)"

    satellite = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 21,
        id: 'mapbox.satellite',
        accessToken: 'sk.eyJ1IjoiaGFyc2hhNjc3MiIsImEiOiJja2o0bDY4MDQwbHgwMnpvOWYyNmtvcGpxIn0.pkPjrHsegZ8wuHMpeJKRvg'
    }).addTo(mymap);

    basemaps = {
        "OSM":osm,
        "Satellite" : satellite
    };

    
    drawnItems  = L.featureGroup().addTo(mymap);
    L.drawLocal.draw.handlers.rectangle.tooltip.start = 'Please draw an area less than 1000 hectares';

    var drawControlFull = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
        },

        draw: {
            polyline:false,
            marker :false,
            circle:false,
            polygon : false,
          /*  {
                allowIntersection:true,
                showArea:true,
                tap:false,
                shapeOptions: {
                    stroke: false,
                    color: 'red',
                    weight: 7,
                    opacity: 0.5,
                    fill: true,
                    fillColor: 'yellow', //same as color by default
                    fillOpacity: 0.4,
                    clickable: true
                }
            }, */
            rectangle : {
                showArea:true
            }
        }
    });

    var drawControlEditOnly = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        },
        draw: false
    });


    mymap.addControl(drawControlFull);

    L.control.layers(basemaps,{'drawLayer':drawnItems}).addTo(mymap);

    var jsonObj
    mymap.on('draw:created', function(e){
        var layer = e.layer;
        drawnItems.addLayer(layer);
        geoJSON_obj = layer.toGeoJSON();
        jsonObj = JSON.stringify(geoJSON_obj);
        $('#user_aoi').val(jsonObj);
        drawControlFull.remove(mymap);
        drawControlEditOnly.addTo(mymap);
    });

    mymap.on('draw:deleted', function(e) {
        drawControlEditOnly.remove(mymap);
        drawControlFull.addTo(mymap);
     });

    $('#sub_id').click(function(){
        var indice_val = $('#indice_list').val()
        console.log(indice_val)
    })
    

    function latlong_string(ll) {
        return "[" + ll.lat.toFixed(3)+ ", "+ ll.lng.toFixed(3)+"]";
    };


    mymap.on('mousemove',function(e) {
        $('#mouse_loc').html(latlong_string(e.latlng))
    });
    /////////////////////////////////////////////////////
})
