from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
#from .models import UploadFileForm
from .final_model_new import *
import os
from json import dumps
import geopandas as gpd
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
import folium
import folium.plugins as plugins

# Create your views here.
def home(request):
    return render(request, 'index1.html')

def ongoing(request):
    #form = UploadFileForm()
    if request.method == 'POST':
        indice_name = request.POST['indices']
        user_aoi_string = request.POST['aoi']
        user_aoi = json.loads(user_aoi_string)

    im = GisGrass(user_aoi,indice_name)
    im.search_sentinel()
    im.aws_download()
    indice_name = im.calc_indice()
    grid_name=im.grid()
    names = {
        'indice_name' : indice_name,
        'grid_name' : grid_name,
    }
    namesJSON = dumps(names)
    return render(request,'index2.html', {'data':namesJSON})
    '''
    folder = os.path.join(os.getcwd(),'media')
    poly = gpd.read_file(fs.open(uploaded_file.name))
    poly['centroid'] = poly['geometry'].centroid
    lat = poly.centroid.map(lambda p: p.x)
    lon = poly.centroid.map(lambda p: p.y)
    m = folium.Map(location = [lon, lat], zoom_start = 12)
    m.add_child(folium.plugins.MeasureControl())
    m.add_child(folium.plugins.Draw())
    m.add_child(folium.plugins.MiniMap(toggle_display=True))
    m.add_child(folium.plugins.MousePosition(prefix="Mouse:", separator=' | '))
    m.add_child(folium.plugins.LocateControl(auto_start=False))
    folium.raster_layers.TileLayer('Stamen Terrain').add_to(m)
    folium.raster_layers.TileLayer('Stamen Toner').add_to(m)
    style_basin = {'fillcolor': '#228B22', 'color':'#228B22'}
    folium.GeoJson(os.path.join(folder,str(uploaded_file.name)),name = 'basin', style_function=lambda x:style_basin).add_to(m)
    folium.LayerControl().add_to(m)
    m = m._repr_html_()
    context = {'my_map': m}
    return render(request,'map.html', context)
    '''
