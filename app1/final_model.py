from .import_statements import *
from .func_req import *
from .indice_tool import *
directory = os.path.join(os.getcwd(),'media/')
indices = ['NDVI','OSAVI','NDMI']

class GisGrass:
    def __init__(self, user_aoi, indice_name): # aoi_name is the user input geojson file
        self.indice_name = indice_name
        self.aoi = user_aoi
        #self.indice = indice_name
        (SEN_U, SEN_P) = ('gis_sensegrass', '123qwe098A')

        # logging
        format = "%(asctime)s: %(message)s"
        logging.basicConfig(filename='logs.log', filemode='a', format=format, level=logging.INFO, datefmt="%H:%M:%S")

        try:
            self.api = SentinelAPI(SEN_U, SEN_P, 'https://scihub.copernicus.eu/dhus')
        except:
            logging.error("Main      --> Credential error")

    def search_sentinel(self):
        footprint = geojson_to_wkt(self.aoi)
        self.aoi_sh = shapely.geometry.Polygon(list(geojson.utils.coords(self.aoi)))
        products = self.api.query(footprint, date=('NOW-120DAYS', 'NOW-0DAYS'), platformname='Sentinel-2', producttype='S2MSI2A',cloudcoverpercentage=(0, 30))
        products_gdf = self.api.to_geodataframe(products)
        self.products_gdf_sorted = products_gdf.sort_values(['ingestiondate'], ascending=[False])

        # Check whether Sentinel data entirely covers user's aoi
        i = 0
        while i < (len(self.products_gdf_sorted)):
            if self.products_gdf_sorted['geometry'][i].contains(self.aoi_sh) == True:
                # api.download(products_gdf_sorted.index[i],directory_path=directory)
                self.meta = self.api.get_product_odata(self.products_gdf_sorted.index[i])
                break
            else:
                i = i + 1
        self.tit = self.meta['title']
        yy = self.tit[11:15]

        if self.tit[15] == '0':
            mm = self.tit[16]
        else:
            mm = self.tit[15:17]

        if self.tit[17] == '0':
            dd = self.tit[18]
        else:
            dd = self.tit[17:19]

        k1 = self.tit[39:41]
        k2 = self.tit[41:42]
        k3 = self.tit[42:44]

        self.aws_tname = 'tiles/' + k1 + '/' + k2 + '/' + k3 + '/' + yy + '/' + mm + '/' + dd + '/0/'
        self.unique_id = self.tit[39:44] + '_' + self.tit[17:19] + '.' + self.tit[15:17] + '.' + self.tit[11:15]

    def aws_download(self):
        s3_client = boto3.Session().client('s3')
        bands = ['B02.jp2', 'B02.jp2', 'B04.jp2', 'B08.jp2']
        
        self.blue_bandpath = directory + 'bands/' + 'blue_'+ self.unique_id  + '.jp2'
        self.green_bandpath = directory + 'bands/' + 'green_'+ self.unique_id  + '.jp2'
        self.red_bandpath = directory + 'bands/' + 'red_'+ self.unique_id  + '.jp2'
        self.nir_bandpath = directory + 'bands/' + 'nir_'+ self.unique_id  + '.jp2'
        self.b8a_bandpath = directory + 'bands/' + 'b8a_'+ self.unique_id  + '.jp2'
        self.b11_bandpath = directory + 'bands/' + 'b11_'+ self.unique_id  + '.jp2'

        print('Searching aws sentinel-1 images')
        

        if self.indice_name == 'NDVI':

            red_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                            # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                            Key=self.aws_tname + 'R10m/B04.jp2',
                                            RequestPayer='requester')
            red_response_content = red_response['Body'].read()
            with open(self.red_bandpath, 'wb') as file:
                file.write(red_response_content)
            
            nir_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                            # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                            Key=self.aws_tname + 'R10m/B08.jp2',
                                            RequestPayer='requester')
            nir_response_content = nir_response['Body'].read()
            with open(self.nir_bandpath, 'wb') as file:
                file.write(nir_response_content)

        elif self.indice_name == 'OSAVI':

            red_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                            # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                            Key=self.aws_tname + 'R10m/B04.jp2',
                                            RequestPayer='requester')
            red_response_content = red_response['Body'].read()
            with open(self.red_bandpath, 'wb') as file:
                file.write(red_response_content)
            
            nir_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                            # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                            Key=self.aws_tname + 'R10m/B08.jp2',
                                            RequestPayer='requester')
            nir_response_content = nir_response['Body'].read()
            with open(self.nir_bandpath, 'wb') as file:
                file.write(nir_response_content)
        
        elif self.indice_name == 'NDMI':
            b8a_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                            # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                            Key=self.aws_tname + 'R20m/B8A.jp2',
                                            RequestPayer='requester')
            b8a_response_content = b8a_response['Body'].read()

            b11_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                            # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                            Key=self.aws_tname + 'R20m/B11.jp2',
                                            RequestPayer='requester')
            b11_response_content = b11_response['Body'].read()

            with open(self.b8a_bandpath, 'wb') as file:
                file.write(b8a_response_content)

            with open(self.b11_bandpath, 'wb') as file:
                file.write(b11_response_content)

        '''
        blue_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                             # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                             Key=self.aws_tname + 'R10m/B02.jp2',
                                             RequestPayer='requester')
        blue_response_content = blue_response['Body'].read()

        green_response = s3_client.get_object(Bucket='sentinel-s2-l2a',
                                              # Key='tiles/7/W/FR/2018/3/31/0/B04.jp2',
                                              Key=self.aws_tname + 'R10m/B03.jp2',
                                              RequestPayer='requester')
        green_response_content = green_response['Body'].read()
        '''
        

        print('response obtained')

        

    def calc_indice(self):
    
        
        if self.indice_name ==  'NDVI':
            aoi_tr = area_of_interest(self.red_bandpath, self.aoi)#AOI transformed to sentinel CRS
            crop_meta = crop_bands_aoi(self.red_bandpath, aoi_tr)[1] #metadata of cropped sentinel bands
            red_cropped = crop_bands_aoi(self.red_bandpath, aoi_tr)[0]#final cropped bands
            nir_cropped = crop_bands_aoi(self.nir_bandpath, aoi_tr)[0]
            NDVI_aoi = calc_ndvi(nir_cropped, red_cropped)
            crop_meta.update({'dtype': NDVI_aoi.dtype})
            save_indice(NDVI_aoi, directory+'indices/', self.indice_name+'_' + self.unique_id + '.tif',crop_meta)
        
        elif self.indice_name ==  'OSAVI':
            aoi_tr = area_of_interest(self.red_bandpath, self.aoi)#AOI transformed to sentinel CRS
            crop_meta = crop_bands_aoi(self.red_bandpath, aoi_tr)[1] #metadata of cropped sentinel bands
            red_cropped = crop_bands_aoi(self.red_bandpath, aoi_tr)[0]#final cropped bands
            nir_cropped = crop_bands_aoi(self.nir_bandpath, aoi_tr)[0]
            OSAVI_aoi = calc_osavi(nir_cropped ,red_cropped)
            crop_meta.update({'dtype': OSAVI_aoi.dtype})
            save_indice(OSAVI_aoi, directory+'indices/', self.indice_name+'_' + self.unique_id + '.tif',crop_meta)

        elif self.indice_name ==  'NDMI':
            aoi_tr = area_of_interest(self.b8a_bandpath, self.aoi)#AOI transformed to sentinel CRS
            crop_meta = crop_bands_aoi(self.b8a_bandpath, aoi_tr)[1] #metadata of cropped sentinel bands
            b8a_cropped = crop_bands_aoi(self.b8a_bandpath, aoi_tr)[0]
            b11_cropped = crop_bands_aoi(self.b11_bandpath, aoi_tr)[0]
            NDMI_aoi = calc_ndmi(b8a_cropped ,b11_cropped)
            crop_meta.update({'dtype': NDMI_aoi.dtype})
            save_indice(NDMI_aoi, directory+'indices/', self.indice_name+'_' + self.unique_id + '.tif',crop_meta)
        '''    
        elif self.indice_name ==  'SIPI':
            SIPI_aoi = calc_sipi(nir_cropped, red_cropped,blue_cropped)
            crop_meta.update({'dtype': SIPI_aoi.dtype})
            save_indice(SIPI_aoi, directory+'indices/', self.indice_name+'_' + self.unique_id + '.tif',crop_meta)
        elif self.indice_name ==  'GCI':
            GCI_aoi = calc_gci(nir_cropped ,green_cropped)
            crop_meta.update({'dtype': GCI_aoi.dtype})
            save_indice(GCI_aoi, directory+'indices/', self.indice_name+'_' + self.unique_id + '.tif',crop_meta)
        '''
        #mcari_aoi = calc_mcari2(nir_cropped, red_cropped, green_cropped) #chlorophyll absorption ratio
        #msavi_aoi = calc_msavi(nir_cropped, red_cropped) # soil attentuated vegetation index
        #mtvi_aoi = calc_mtvi2(nir_cropped, red_cropped, green_cropped) # modofied triangular veg index
        #save_indice(NDVI_aoi, directory+'indices/', self.indice_name+'_' + self.unique_id + '.tif',crop_meta)
        #save_indice(mcari_aoi, directory+'indices/', 'MCARI_' + self.unique_id + '.tif',crop_meta)
        #save_indice(msavi_aoi, directory+'indices/', 'MSAVI_' + self.unique_id + '.tif',crop_meta)
        #save_indice(mtvi_aoi, directory+'indices/', 'MTVI_' + self.unique_id + '.tif',crop_meta)
        raster_reproject(directory +'indices/', self.indice_name + '_'+ self.unique_id + '.tif',  self.indice_name + '_' + self.unique_id + '_proj' + '.tif')
        #raster_reproject(directory +'indices/', 'MCARI_' + self.unique_id + '.tif', 'MCARI_' + self.unique_id + '_proj' + '.tif')
        #raster_reproject(directory +'indices/', 'MSAVI_' + self.unique_id + '.tif', 'MSAVI_' + self.unique_id + '_proj' + '.tif')
        #raster_reproject(directory +'indices/', 'MTVI_' + self.unique_id + '.tif', 'MTVI_' + self.unique_id + '_proj' + '.tif')
    
    def grid(self):
        self.row = 3
        self.col = 3
        polygons = create_grid(self.aoi, self.row, self.col)
        grid = gpd.GeoDataFrame({'geometry': polygons})
        grid = grid.set_crs(epsg=4326)
        grid_bound = grid.boundary

        

        if self.indice_name ==  'NDVI':
            bare = []
            unhealthy = []
            moderate = []
            healthy = []
            with rio.open(directory+'indices/' +  self.indice_name + '_' + self.unique_id + '_proj' + '.tif') as src1:
                no_data1 = src1.nodata
                for i in range(self.row*self.col):
                    out_image1, out_transform1 = mask(src1, grid_bound[i:i + 1].values, crop=True, all_touched=False, filled=False)
                    data1 = out_image1.data[0]
                    row1, col1 = np.where(data1 != no_data1)
                    ndvi = np.extract(data1 != no_data1, data1)
                    bare.append(len(np.where((ndvi > -0.1) & (ndvi <=0.1))[0]))
                    unhealthy.append(len(np.where((ndvi > 0.1) & (ndvi <= 0.35))[0]))
                    moderate.append(len(np.where((ndvi > 0.35) & (ndvi <= 0.65))[0]))
                    healthy.append(len(np.where((ndvi > 0.65) & (ndvi <= 1))[0]))
            grid = gpd.GeoDataFrame({'geometry': polygons, 'bare_Soil': bare, 'unhealthy_veg':unhealthy, 'moderate':moderate, 'healthy_veg':healthy})
            grid = grid.set_crs(epsg=4326)
            grid.to_file(directory +'indices/'+ 'ndvi_grid_' + self.unique_id + '.geojson', driver='GeoJSON')


        elif self.indice_name ==  'NDMI':
            bare = []
            high = []
            moderate = []
            no = []
            with rio.open(directory+'indices/' +  self.indice_name + '_' + self.unique_id + '_proj' + '.tif') as src1:
                no_data1 = src1.nodata
                for i in range(self.row*self.col):
                    out_image1, out_transform1 = mask(src1, grid_bound[i:i + 1].values, crop=True, all_touched=False,
                                                  filled=False)
                    data1 = out_image1.data[0]
                    row1, col1 = np.where(data1 != no_data1)
                    ndmi = np.extract(data1 != no_data1, data1)
                    bare.append(len(np.where((ndmi >= -1) & (ndmi <=-0.35))[0]))
                    high.append(len(np.where((ndmi >= -0.2) & (ndmi <= 0.2))[0]))
                    moderate.append(len(np.where((ndmi > 0.2) & (ndmi <= 0.5))[0]))
                    no.append(len(np.where((ndmi > 0.5) & (ndmi <= 1))[0]))
            grid = gpd.GeoDataFrame({'geometry': polygons, 'bare_Soil': bare, 'high_waterstress':high, 'moderate_waterstress':moderate, 'healthy_veg':no})
            grid = grid.set_crs(epsg=4326)
            grid.to_file(directory +'indices/'+ 'ndmi_grid_' + self.unique_id + '.geojson', driver='GeoJSON')


        elif self.indice_name ==  'OSAVI':
            bare = []
            unhealthy = []
            moderate = []
            healthy = []
            with rio.open(directory+'indices/' +  self.indice_name + '_' + self.unique_id + '_proj' + '.tif') as src1:
                no_data1 = src1.nodata
                for i in range(self.row*self.col):
                    out_image1, out_transform1 = mask(src1, grid_bound[i:i + 1].values, crop=True, all_touched=False,
                                                  filled=False)
                    data1 = out_image1.data[0]
                    row1, col1 = np.where(data1 != no_data1)
                    osavi = np.extract(data1 != no_data1, data1)
                    bare.append(len(np.where((osavi > -0.1) & (osavi <=0.1))[0]))
                    unhealthy.append(len(np.where((osavi > 0.1) & (osavi <= 0.35))[0]))
                    moderate.append(len(np.where((osavi > 0.35) & (osavi <= 0.65))[0]))
                    healthy.append(len(np.where((osavi > 0.65) & (osavi <= 1))[0]))
            grid = gpd.GeoDataFrame({'geometry': polygons, 'bare_Soil': bare, 'unhealthy_veg':unhealthy, 'moderate':moderate, 'healthy_veg':healthy})
            grid = grid.set_crs(epsg=4326)
            grid.to_file(directory +'indices/'+ 'osavi_grid_' + self.unique_id + '.geojson', driver='GeoJSON')



    
    def final(self):
        indice_path = directory+'indices/' +  self.indice_name + '_' + self.unique_id + '_proj' + '.tif'
        #mcari_path = directory+'indices/'  + 'MCARI_' + self.unique_id + '_proj' + '.tif'
        #msavi_path = directory + 'indices/' + 'MSAVI_' + self.unique_id + '_proj' + '.tif'
        
        
        with rio.open(indice_path) as src1:
            boundary1 = src1.bounds
            b_new1 = [[boundary1[3], boundary1[0]], [boundary1[1], boundary1[2]]]
            img1 = src1.read(1)
        #with rio.open(mcari_path) as src2:
            #boundary2 = src2.bounds
            #b_new2 = [[boundary2[3], boundary2[0]], [boundary2[1], boundary2[2]]]
            #img2 = src2.read(1)
        lat_c = (boundary1[3] + boundary1[1]) / 2
        lon_c = (boundary1[0] + boundary1[2]) / 2

        f = branca.element.Figure()
        #m1 = folium.Map(location=[lat_c, lon_c], tiles='OpenStreetMap', zoom_start=13,
                        #control_scale=True)
        m1 = folium.Map(location=[lat_c, lon_c], tiles= None, zoom_start=12,control_scale=True)
        folium.raster_layers.TileLayer(tiles='https://api.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=sk.eyJ1IjoiaGFyc2hhNjc3MiIsImEiOiJja2o0bDY4MDQwbHgwMnpvOWYyNmtvcGpxIn0.pkPjrHsegZ8wuHMpeJKRvg',
                                       name = 'Satellite',attr='Mapbox',overlay = True, show = True).add_to(m1)
        
        folium.raster_layers.TileLayer('OpenStreetMap').add_to(m1)
        # folium.raster_layers.TileLayer('MapQuest Open Aerial').add_to(m1)
        folium.raster_layers.TileLayer('Stamen Toner').add_to(m1)
        #folium.raster_layers.TileLayer('Stamen Terrain').add_to(m1)
        m1.add_child(folium.plugins.MeasureControl())
        m1.add_child(folium.plugins.Draw())
        m1.add_child(folium.plugins.MiniMap(toggle_display=True))
        m1.add_child(folium.plugins.MousePosition(prefix="Mouse:", separator=' | '))
        m1.add_child(folium.plugins.LocateControl(auto_start=False))
        m1.add_child(
            folium.plugins.Fullscreen(position='topleft', title='Full Screen', title_cancel='Exit Full Screen'))
        #m1.add_child(folium.raster_layers.ImageOverlay(img2, name='MCARI', colormap=cm.get_cmap('RdYlGn'), opacity=1,
                                                       #bounds=b_new1, interactive=True, zindex=1))
        

        #ndvi_cm = branca.colormap.LinearColormap(['red','yellow','green'], vmin=-1, vmax=1, caption='NDVI')
        #ndmi_cm = branca.colormap.LinearColormap(['red','yellow','blue'], vmin=-1, vmax=1, caption='NDMI')
        #osavi_cm = branca.colormap.LinearColormap(['red','yellow','green'], vmin=-1, vmax=1, caption='NDVI')
        #m1.add_child(folium.raster_layers.ImageOverlay(img1, name=self.indice_name, colormap=cm.get_cmap('RdYlGn'), opacity=1,
                                                       #bounds=b_new1, interactive=True, zindex=1))
       


        if self.indice_name ==  'NDVI':
            #ndvi_cm = branca.colormap.LinearColormap(['red','yellow','green'], vmin=img1.min(), vmax=img1.max(), caption='NDVI')
            ndvi_cmap = branca.colormap.LinearColormap(colors=['red', 'yellow', 'green'], vmin=img1.min(), vmax=img1.max(), caption='NDVI')
            ndvi_cmap.add_to(m1)
            m1.add_child(folium.raster_layers.ImageOverlay(img1, name=self.indice_name, colormap=cm.get_cmap('RdYlGn'), opacity=1,
                                                       bounds=b_new1, interactive=True, zindex=1))
            grid_json = read_geojson(directory +'indices/'+ 'ndvi_grid_' + self.unique_id + '.geojson')
            grid_gpd  = gpd.read_file(directory +'indices/'+ 'ndvi_grid_' + self.unique_id + '.geojson')
            for j in range(self.row*self.col):
                tot = grid_gpd['bare_Soil'][j]+ grid_gpd['unhealthy_veg'][j]+ grid_gpd['moderate'][j]+ grid_gpd['healthy_veg'][j]
                field = {'bare_soil': (100 * grid_gpd['bare_Soil'][j]) / tot, 'unhealthy_veg': (100 * grid_gpd['unhealthy_veg'][j]) / tot,
                'moderate_healthy': (100 * grid_gpd['moderate'][j])/ tot ,'healthy_veg': (100 * grid_gpd['healthy_veg'][j]) / tot}
                grp_bar_chart = vincent.GroupedBar(field, width=450, height=350)
                grp_bar_chart.axis_titles(x='Landcover', y='Percentage')
                # grp_bar_chart.legend(title='Class')
                # grp_bar_chart.colors(brew='Set2')
                gb_c_json = grp_bar_chart.to_json()
                popup1 = folium.Popup()
                folium.Vega(gb_c_json, width='100%', height='100%').add_to(popup1)
                lonlat = grid_json[j]['geometry']['coordinates'][0]
                lonlat_t = np.transpose(lonlat)
                latlon = np.dstack((lonlat_t[1], lonlat_t[0]))
                m1.add_child(folium.vector_layers.Polygon(latlon, popup=popup1, fill=True,
                                                      fill_opacity=0.0, color='black', weight=0.3))

        elif self.indice_name ==  'NDMI':
            #ndmi_cm = branca.colormap.LinearColormap(['red','yellow','blue'], vmin=img1.min(), vmax=img1.max(), caption='NDMI')
            ndmi_cmap = branca.colormap.LinearColormap(colors=['red', 'yellow', 'Blue'], vmin=img1.min(), vmax=img1.max(), caption='NDMI')
            ndmi_cmap.add_to(m1)
            m1.add_child(folium.raster_layers.ImageOverlay(img1, name=self.indice_name, colormap=cm.get_cmap('RdYlBu'), opacity=1,
                                                       bounds=b_new1, interactive=True, zindex=1))
            grid_json = read_geojson(directory +'indices/'+ 'ndmi_grid_' + self.unique_id + '.geojson')
            grid_gpd  = gpd.read_file(directory +'indices/'+ 'ndmi_grid_' + self.unique_id + '.geojson')
            for j in range(self.row*self.col):
                tot = grid_gpd['bare_Soil'][j]+ grid_gpd['high_waterstress'][j]+ grid_gpd['moderate_waterstress'][j]+ grid_gpd['healthy_veg'][j]
                field = {'bare_soil': (100 * grid_gpd['bare_Soil'][j]) / tot, 'high_waterstress': (100 * grid_gpd['high_waterstress'][j]) / tot,
                'moderate_waterstress': (100 * grid_gpd['moderate_waterstress'][j]) / tot ,'healthy_veg': (100 * grid_gpd['healthy_veg'][j]) / tot}
                grp_bar_chart = vincent.GroupedBar(field, width=450, height=350)
                grp_bar_chart.axis_titles(x='Landcover', y='Percentage')
                # grp_bar_chart.legend(title='Class')
                # grp_bar_chart.colors(brew='Set2')
                gb_c_json = grp_bar_chart.to_json()
                popup1 = folium.Popup()
                folium.Vega(gb_c_json, width='100%', height='100%').add_to(popup1)
                lonlat = grid_json[j]['geometry']['coordinates'][0]
                lonlat_t = np.transpose(lonlat)
                latlon = np.dstack((lonlat_t[1], lonlat_t[0]))
                m1.add_child(folium.vector_layers.Polygon(latlon, popup=popup1, fill=True,
                                                      fill_opacity=0.0, color='black', weight=0.3))


        elif self.indice_name ==  'OSAVI':
            #osavi_cm = branca.colormap.LinearColormap(['red','yellow','green'], vmin=img1.min(), vmax=img1.max(), caption='OSAVI')
            osavi_cmap = branca.colormap.LinearColormap(colors=['red', 'yellow', 'green'], vmin=img1.min(), vmax=img1.max(), caption='OSAVI')
            osavi_cmap.add_to(m1)
            m1.add_child(folium.raster_layers.ImageOverlay(img1, name=self.indice_name, colormap=cm.get_cmap('RdYlGn'), opacity=1,
                                                       bounds=b_new1, interactive=True, zindex=1))
            grid_json = read_geojson(directory +'indices/'+ 'osavi_grid_' + self.unique_id + '.geojson')
            grid_gpd  = gpd.read_file(directory +'indices/'+ 'osavi_grid_' + self.unique_id + '.geojson')
            for j in range(self.row*self.col):
                tot = grid_gpd['bare_Soil'][j]+ grid_gpd['unhealthy_veg'][j]+ grid_gpd['moderate'][j]+ grid_gpd['healthy_veg'][j]
                field = {'bare_soil': (100 * grid_gpd['bare_Soil'][j]) / tot, 'unhealthy_veg': (100 * grid_gpd['unhealthy_veg'][j]) / tot,
                'moderate_healthy': (100 * grid_gpd['moderate'][j]) / tot ,'healthy_veg': (100 * grid_gpd['healthy_veg'][j]) / tot}
                grp_bar_chart = vincent.GroupedBar(field, width=450, height=350)
                grp_bar_chart.axis_titles(x='Landcover', y='Percentage')
                # grp_bar_chart.legend(title='Class')
                # grp_bar_chart.colors(brew='Set2')
                gb_c_json = grp_bar_chart.to_json()
                popup1 = folium.Popup()
                folium.Vega(gb_c_json, width='100%', height='100%').add_to(popup1)
                lonlat = grid_json[j]['geometry']['coordinates'][0]
                lonlat_t = np.transpose(lonlat)
                latlon = np.dstack((lonlat_t[1], lonlat_t[0]))
                m1.add_child(folium.vector_layers.Polygon(latlon, popup=popup1, fill=True,
                                                      fill_opacity=0.0, color='black', weight=0.5))

        folium.LayerControl().add_to(m1)
        #ndvi_colormap = linear.RdYlGn_03.scale(img1.min(), img1.max())
        #ndvi_colormap.add_to(m1)
        f.add_child(m1)
        outfp2 = directory +'result/'+ self.indice_name + '_map_' + self.unique_id + '.html'
        f.save(outfp2)
        return m1

    
