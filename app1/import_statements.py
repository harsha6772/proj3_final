import folium
import folium.plugins as plugins
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
import pandas as pd
import os, glob, json, logging
from shapely.geometry import Polygon
import geopandas as gpd
import rasterio as rio
from rasterio.warp import calculate_default_transform, reproject, Resampling
from rasterio.mask import mask
import earthpy.spatial as es
import earthpy.plot as ep
import shapely
from shapely import geometry
import geojson
import vincent
import branca
#from zipfile import ZipFile
import matplotlib
from matplotlib import cm
#import matplotlib.pyplot as plt
import numpy as np
from branca.colormap import linear
import boto3
import threading
from datetime import datetime
import requests
import os, glob, json, logging