import geopandas as gpd
import rasterio as rio
import earthpy.spatial as es
import numpy as np
from rasterio.warp import calculate_default_transform, reproject, Resampling
from shapely.geometry import Polygon


#Load AOI and transform its CRS to Sentinel Image CRS
def area_of_interest(reference_bandpath,aoi_vectorpath):
    #aoi = gpd.read_file(aoi_vectorpath)
    aoi_temp = { "type": "FeatureCollection", "features": [aoi_vectorpath]}
    aoi = gpd.GeoDataFrame.from_features(aoi_temp)
    aoi = aoi.set_crs(epsg=4326)
    with rio.open(reference_bandpath) as src:
        aoi_tr = aoi.to_crs(src.crs)
    return aoi_tr

#Crop Sentinel image bands to the extent of AOI
def crop_bands_aoi(in_bandpath, aoi_tr):
    band = rio.open(in_bandpath)
    band_crop , band_crop_meta = es.crop_image(band,aoi_tr)
    band_crop = np.squeeze(band_crop)
    band_ov = band_crop,band_crop_meta
    return band_ov

def save_indice(indice,out_directory,out_name, crop_meta):
    with rio.open(out_directory+out_name, 'w', **crop_meta) as ff:
        ff.write(indice,1)

dst_crs = 'EPSG:4326'
def raster_reproject(directory_in, directory_out,in_raster,out_raster):
    with rio.open(directory_in+in_raster) as src:
        transform, width, height = calculate_default_transform(
        src.crs, dst_crs, src.width, src.height, *src.bounds)
        kwargs=src.meta.copy()
        kwargs.update({
        'crs':dst_crs,
        'transform': transform,
        'width': width,
        'height': height
        })
        with rio.open(directory_out+out_raster, 'w', **kwargs) as dst:
            reproject(source=rio.band(src, 1),
            destination=rio.band(dst, 1),
            src_transform=src.transform,
            src_crs=src.crs,
            dst_transform=transform,
            dst_crs=dst_crs,
            resampling=Resampling.nearest)

#print('creating grid')
def create_grid(polygon_path,cols,rows):
    aoi_temp = { "type": "FeatureCollection", "features": [polygon_path]}
    aoi1 = gpd.GeoDataFrame.from_features(aoi_temp)
    aoi1 = aoi1.set_crs(epsg=4326)
    #aoi1 = gpd.read_file(polygon_path)
    boundary= aoi1.bounds
    lon_min,lat_min,lon_max,lat_max = boundary['minx'][0],boundary['miny'][0],boundary['maxx'][0],boundary['maxy'][0]
    lon_step= np.divide((lon_max-lon_min),cols)
    lat_step= np.divide((lat_max-lat_min),rows)
    polygons = []
    for j in range(cols):
        for i in range(rows):
            a=lon_min+j*lon_step
            b=lat_min+i*lat_step
            polygons.append(Polygon([(a,b), (a+lon_step,b),(a+lon_step,b+lat_step),(a,b+lat_step)]))
    return polygons
