import numpy as np


def calc_ndvi(nir ,red):
    '''Calculate Normalized Difference Vegetation Index from integer arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    ndvi = np.divide(nir-red, nir+red)
    ndvi = np.where(ndvi == np.inf, 0, ndvi)
    return ndvi

def calc_ndmi(b8a , b11):
    # Calculate Normalized Difference Water Index from integer arrays
    nir = b8a.astype('f4')
    green = b11.astype('f4')
    ndmi = (b8a - b11) / (b8a + b11)
    return ndmi

def calc_ndwi(nir ,green):
    # Calculate Normalized Difference Water Index from integer arrays
    nir = nir.astype('f4')
    green = green.astype('f4')
    ndwi = (green - nir) / (nir + green)
    return ndwi

def calc_nli(nir ,red):
    '''Calculate Non Linear Index from integer arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    nli = (np.power(nir ,2) - red) / (np.power(nir ,2) + red)
    nli = np.nan_to_num(nli)
    return nli

def calc_savi(nir ,red ,L):
    '''Calculate Soil Adjusted Vegetation Index from integer arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    savi = (nir - red) * (1 + L ) /(nir + red + L)
    return savi

def calc_osavi(nir ,red):
    '''Calculate Optimized Soil Adjusted Vegetation Index from integer arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    osavi = (nir - red) * (1.6 ) /(nir + red + 0.16)
    return osavi

def calc_msavi(nir ,red):
    '''Calculate Modified Soil Adjusted Vegetation Index arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    msavi = nir + 0.5 - (0.5 * np.sqrt((2 * nir + 1 )**2 - 8 * (nir - (2 * red))))
    return msavi

def calc_msavi2(nir ,red):
    '''Calculate Modified Soil Adjusted Vegetation Index 2 arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    msavi2 = (2 * (nir + 1) - np.sqrt((2 * nir + 1 )**2 - 8 * (nir - red)) ) /2
    return msavi2

def calc_mtvi2(nir ,red ,green):
    '''Calculate Modified Triangle Vegetation Index 2 arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    green = green.astype('f4')
    mtvi2 = (1.8*(nir-green) - 3.75 *(red -green)) / np.sqrt((2 * nir + 1) ** 2 + (6 * nir - np.sqrt(red)) - 0.5)
    return mtvi2


def calc_mcari2(nir, red, green):
    '''Calculate Modified Chlorophyll Absorption Ratio Index 2 arrays'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    green = green.astype('f4')
    mcari2 = (3.75 * (nir - red) - 1.95 * (nir - green)) / np.sqrt((2 * nir + 1) ** 2 + (6 * nir - np.sqrt(red)) - 0.5)
    mcari2[mcari2 == 0] = np.nan
    return mcari2


def calc_gci(nir, green):
    '''Calculate Green Chlorophyll Index'''
    nir = nir.astype('f4')
    green = green.astype('f4')
    gci = nir / (green - 1)
    return gci


def calc_sipi(nir, red, blue):
    '''Calculate Structure Insentitive Pigment Index'''
    nir = nir.astype('f4')
    red = red.astype('f4')
    blue = blue.astype('f4')
    np.seterr(divide='ignore', invalid='ignore')
    sipi = (nir - blue) / (nir - red )
    return sipi

